print("Programa para verificar se uma letra é uma 'VOGAL':\n")

vogal = input("Digite uma letra: ")

vogal = vogal.lower()

if(vogal == 'a' or vogal == 'e' or vogal == 'i' or vogal == 'o' or vogal == 'u'):
    print("É uma vogal.")

else:
    print("Não é uma vogal.")
